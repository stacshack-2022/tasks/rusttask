use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opts {
    value: i32,
}

const p: i32 = 7;
const log_image : &str = "
kkkkkkkkkkkkkkkkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkkkxxkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxxxxxxxxxxxxxxxxxxxxkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkxxkkkxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxxkkkkxxkkkkkkkkkkkkxxxxxxxxxxxxxxxxxdodxxxxxxxxxxxxxxxxxxxxkkkxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxxxxxxxxxxxxxxoldxxkdooxkkxxxxxxxxxxxxxxxxkkkxxxxxxxxxxxxxxxxxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOO
kkkkkkkOOOOkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxxxxxxkxxxoldxxxoclxkkkkxxxxxxxxxxxxxkkkkkkkkxxxxxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOO
kOOOOOOOOOOOOOOOOkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxoodoolccoxkkkkkkxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOO
kOOOOOOOOOOOOOOOOOOOOOOkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxoloooooloxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOkkkkkkOOOOOOOOOkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxllddodocoxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOO0
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkkkkkkkkkkkkkkkkkkkkkkkkxlldddxolokkkxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO0000O00
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkkkkkkkkkkkkkkkkkkkkkkkxlldxxolcokkdlldkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO00000000
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkkkkkkkkkkkkkkkdolcllllcccldxocldkkxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO00000000
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkkkOOOkkkkkkkkkkkkkkOxlccccoddlccloxocldkkdldkOkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO00000000
kkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkkkOOOkkkkkkkkkkOOOOOxlcccodoolccldxoclxkxocdkOkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO0
kkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkkkOOOOkkkkkkkkkOOOOOxccccllllc:cldxoclddoclxOkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
kkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkOOOOOkkkkkOOOOOOOdc::ccodoccclodlcldxdlcdkkkkOkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO00
kkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOkkkkOOOOOOOkxddxkOOOOOOkkkkOOOOOOkd:::cccc:::ccodlclodxocdOOOOOkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
kkkkkkkOOOOOOOOOOOOOOOOOOOOOOkkkkkOOOOOkkkkkkkkxdolc::clodxkkOOkkkOOOOkOkl:::::cccc:ccddl:coxxlcdOOOOOOOkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO00
kkkkkkkkkkkkkkkOOOOOOOOOOOOOkkkkkkkkkkkkkkkkkkoc::::::ccc:cclodxkkkxdolll::::cc:ccccccodc:lxxoccdOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO000
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxloddoc::cccccccc:ccccc::::::::clccccccccodc:cddoccdOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO0000
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxldkkko::::::::cccccc::::::::::ccccllc:ccddc:lxxdlcdOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO0000000000OOOO000000
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkdlxOkkl:::;;:c::cccc:::::::;:::::c:::::cldoc:codxlcdOOOOOkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO00000000OOOOO0OOO0OO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkolxkkxc;:::cc:::::::::::::::::::::::::llllc::cdxdlcdkkkOkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOkkkkkkkkkkkkOOOOOOOOOOOkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxookkkdccccllc::::::::::;;;;:::::::::::cloddoolllcccdkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxldkkkoclllllc::::;;::::::;;;;:::::::::::::codxxdolcoxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOO000
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkdlxkkxlclllllc::::::::;;::cccc:::::::::::::;;:coxxkdodxxkkkkkkkkkkkkxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOO0000
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkolxkkdlcllllc:::::;;:;;::cccccccccc::::;::::;;;;:codlcoxkkkxxxxdxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOO000OOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOkkkkxlokkkdcccccc:;;;;;;;;;;:;;;::::ccccccc:::::::;;:;;;:::odooolcccccllodxxxxxkkkkxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkdldkkko:::::;;;;;;;;;;;:;;;;;;;:::ccccccllc:::::::;;;;,;;;;::cc::clolooddxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOOOOOOOOOOOOOO0
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkolxkkxl::;;;;;;;;;;;;;::;::;;;:::;::::::ccccc::;;;;;;;,,,;;,;;;;;;;;::::lodxdddxkxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOxdxOOOOOOOOOOOO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxlldolc:;:;;;;;;;;::;;;:;;::;:;::;;;:;;;;;:::::::;;;;;;,,,,,,;,;;;;,,,,,;;:cldddxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOo:lxOOOOOOOOxkO
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxlc:::;;,,,,,,,;;;;;::;;;;;;:;;;;::;;::;;:;;;::::::::;;;,,,,,,,,,;;;,,,,,;;;;;:clodxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOOkxdool::cllllooddlok
looxxxkkkkkkkkkkkkkkkkkkkkkkkkkkxxdddxxxxo:;:::;,,,,,,,;;;;;;;;;;;;;:;;;:::::::;;:;;::::;;;;;;;;;;;,,,,;,,,,,,,,;;;;;;;;::cldxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkdodoc:::::::::::::;:::cx
:;:loddxxxxxkkkkkkkkkkkkkkkkkkkkxl:;:ccccl:;clc;,,,,,,;::;;;;;;;;;;;:;;;::::::::::;::c::::::::;;;::::;;;,,,,;,,,,;;;;;;;;;;;::loxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxc:::::::::::::::;;;;cco
dl;;:::clcccclllllooddxxxxkkkkkkxc;;;c:,,;;;;;;,,;,,,,:c:;;;;;;;;;;::;;;:::;:::::::::cc::::c:::;:::::::,,,,;;;;;;;;;;;;;;;;:;;;::codxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkOkkOOOOOOkoc:::::::::::::;;;;:ccl
dl::::;:::;,,;,''',,,,,;;:clollll:,;;;;,,,;;;;,,,,;;,,:::;;;;;;;;;;;;;::::::;:::;;;:cc::c::lc:::::::cc:,,,;:;;::::;;;;;;;;;;;;;;;;::coddxkkkkkkkkkkkkkkkkkkkkkkkkkkOOOOOOOOOOOOOOdlc:::::::c:::::;;:clll
dooooolcc::;;;,,,'',''''',;::,,,,,,,;;;;;;;;;,,,;::;,;:c:;;;;;;,,,,,,,,;;:::::::ccc:c::;;;;;;;;;;;;;:::,,;::::cccc:cc:;;;;;;;;;;;;:;;;;;:lodxkxkkkkkkkkkkkkkkkkkkkkkkkkkOOOkkkkkOxl::::::::cc::::::::::c
dooooolccc:;;,,,,',,''''',;;;,,,;;;;;;;;;;:::;,;:c:;,;:::::;;,,;,,,,,,,,,,,;;;:cllllc:;;;;;;:;;;::::ccc;;;clllooolcclc::;;;;;;;;;;;;;;;;;:clodxxkxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkocc:::::::ccllcc::c:cc
lccclcccc:;;,,,,,,,'''''',,,,;;;:;;;;;;;;;;::;,;:::;;:c:;;;;;,,;;;;;::::;;;;;,:clllcccccc:::::;::cccccc:;::cccllcc::c::::::;;;;;;;;;;;;:::::cclodxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkdlc:cc:cccccc::::ccc::
llcccccc:;;,,,,,',,'''''',,,,;;;;;;;;;:;:::::;,;:::;;:c:;;;;;,,cc::;:cc:::;;;,:clllcccclcccc:::::cclcll:::::::::::::ccccccc:cc::;;::::::::::c::ccclodxxkkkkkkkkkkkkkkkkkkkkkkkkkkkxlcccc::ccccccc:ccccc:
ccccccc:;;;;,,;,',,''',,,,,,;;;;;;;;;;;;::;;;;;:c::;;cc:;;::,';clcc::cccccc::;coooooolllllccc:::;:cccllc::;:ccccc:::cllllllccccclc::;::::::::c:::::cccloodkkkkkkkkkkkkkkkkkkkkkkkkxolc:cc:cccccclccccccc
ccccc::;;;;;,;;,,,,,,,,,,,,,;;:::;;;;;;;;:;;;;;:c::::ccc::c:,,;::::cclllloolcclooddooolllc:::::;;:cccllc:;::cccc::::clclllcclcccddoolc:::::::::::::::::::codxxxxxxxxxxxxxxxxxxxxxxxdlccccccclllllllcllll
;;;;;,,;;;;;;;;,;;,,,;,,,,,,,,;::;:::cc::::::;:ccccccccccccc:::cccclloolloolccloooollllllc:::::::ccllllc:::cccc:::::clllolllllclolllool:::::::::cc:::::::::cloddoloddddxxxxxxxxxddddollllllloloooooolloo
;;;;;;;;:::::::ccccccllllc::;;:ccclodkdcoodolloddddxxddxxxxxxxxxxxxkkxxxxxxxddddoolllllc::::::ccccllllllcccccccccccclllllllllollooloooolllllcccccccccccllccccclllooooodddddddddddddddooollloooooodoooooo
::;;;;::ccccc:cc::clloddoooollooodddk0kdxkOOkxkOOkkkkxkOOkkkkkkkOOOOOOkOOkOOOkkkxxdolooc:::cclllllolooolllllllllllllloooooooddooddoddddodddddoooooooooooooooooooodddddddddddddddddddddoooooodddddddddddd
::::::clollllccccclodxxxxxddddxxxxxkkOkkkkOOkkkOOkkkkkkOOOkkkkkkkkkkOkkkkkkOkkkkkkxddddoooooooooodddddddooooooodddddddddddxxxxddxxxxxxdddxxxxxxdddddddddddddddddddddddddxxxxxdddddddddxxdddddddddddddddd
lllllloddddddddddddxxxxxxxxxxxxxkxxxkxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxxxxxxxxdddddxxxdddddddddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxddddddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddd
loooodxxxxxddddxxxxxxxxxxxxxxxxxxxxxxxxxxkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddddxxdxxxxxxxxxxxxxxxxxxxxxkkxxxxxxxxxxddodd
looooodddddoddoodddxxxxxxxdddddddddddddddxxxxxkkkkkkkkkkkkkkkkkkkkkkxxxkkxkkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxddxxdddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddddddddddddxxxxxxxxxxxxxxxxxxxddddddddooooo
lllllllllllloooooooooddddodddddddddddddxxxxxxxkkkkkkkkkkkkkkkkkxxxxxxxxxxxxxkxxxxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxddxxdddddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxkkkkkOkkkkOOOOkxxddoddddddddoooooood
ccccclllllllooodddoddddddddddxxxddddddxxxxkkkkxxxxxxkkkkkkxxxxxxxxxxxxxxxxxxxxxdxxxxxxxxxxxxxxxddxxdddddxxxxxxxxxxxxxxxxdoooddxxxxxxxxddxxxxxxkkkxxxxkkkkkkkOOkOOOOkkkkkkkkkkkkxxddoooooooooooddddddxkkO
ddxxxxxddxxxxxkkkkkxxxddxxxxxxxxxxxxxxxxxxkkkkkxxxxxxxxkkkxxxxxxxxxxxxxkkxxxxxxkkkkkxxxxxxxxxxxxddxxxxkkkxxxdxxxkkkkxxxxxxxxxxxkkxxxxxxxxxkkkkkkkkkkxxxxxxkkkOOOOOOOOkxxxxdooollloollllooddxxxkkkkkkkkOO
kOOOOkkkkkxxxxxxkxxxxxxxkxxxxxxxxkkkkxxxxxxkkkkkkxxkkkkkkkkxxxxxxxxddxxkkkxxxxxxxxxxkkkkxxxxxkkxdddddxxkkkxxxxxxkkkkkkxxxdxxkkkkkxxxxxxdddxxxxxkkkkkxxxxdxxkkkkkkOOOkxdollcccccllooddxxkkkkkkkkkkkkkkkkk
dxkOkkxddddodddxkxxxxxxxxxxxdxxddxkkxxxxxxkkkxddooodxkkkkkkxxxxxddddxxkkkkkkkxxxxxxxxkkxxxxkkkkkxddddddxkkkxxxxxxxxkkkkxxxxkkkkkkkOkkkkkxxxxxxxkkkkkxxxxxxkkxxxxxddolcccccllodddxkkkkkkkkkkkkOOOOO00OOOO
odddoolloollodxkkkxxddddxxxdooodddxxxxxxxxxdolloddoloxkkkxkxxxxdodddxkkxkkkxxdxxxxkkkkxxxxxxxxxxxxkxxxxxxkkkxxxxxxkOOkkkkxxxkkkkkkkkkkkkkkkkkkkkkkkkkxxxxkkkkkkkkxddddddddxxxxxxxxxxkkkkxxxxxxkkxkkkkkOO
dooollloollodxddxxxolloxxdoooodxdddxxxkkxxxdooodxkxooddxkkxkxxxdooodxxxxxxdddddoddxxxxxxddxxxxxxxxxxkkxxxxkkkkkxxkkOOkkkOOOOOOOOkkkkkxxxxxxkkkkkkkkkkkkkkxxxxdxxxxxkkkkxxxxxxxxxxxkkkkkkkxxdxxxxxxxdodxx
kxxddooolcoxxddooodoccoxxxdooloddddddxkxdxxxxlcodddoooodxxxxxdddooooddddoooddddoloddxxdddddddddkkkxxxkkxxxxxxxxxkkxxkkkkkkkkOkkkOOkkkkxxxxxxkxdddddddxxxdddddoodooodddxddxxxkOOOOOkxxxxxdxxxdddxxxxkdood

";

fn main() {
    let opts = Opts::from_args();
    let mut value = opts.value;
    let mut count = 0;
    while value % p == 0 {
        count += 1;
        value /= p;
    }
    print!("{},{},{}", p, count, log_image);
}
