FROM rust

RUN mkdir /var/check-7
WORKDIR /var/check-7

COPY ./ ./

CMD [ "./main" ]